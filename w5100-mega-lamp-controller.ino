#include <ArduinoJson.h>


// w5100
#include <SPI.h>
#include <EthernetUdp.h>
#include <Ethernet.h>
#include "WebServer.h" // https://github.com/sirleech/Webduino/


byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 177);

// udp
unsigned int udpPort = 8888;      // local port to listen on

//tcp WebServer
#define PREFIX ""
WebServer webserver(PREFIX, 808);

// define variables to store web server post data
#define NAMELEN 32
#define VALUELEN 32
char name[NAMELEN];
char value[VALUELEN];



// buffers for receiving and sending UDP data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];
char  ReplyBuffer[] = "acknowledged";

EthernetUDP Udp;



#include <OSCBundle.h>
#include <OSCBoards.h>
OSCErrorCode error;


// // ESP8266 OSC Stuff
// #include <OSCMessage.h>
// #include <OSCBundle.h>
// #include <OSCData.h>
// OSCErrorCode error;


#include <FastLED.h>

// // Uncomment for use with ESP8266
//
// #include <ESP8266WiFi.h>
// #include <WiFiUdp.h>
// #include <ESP8266WebServer.h>
// #include <ESP8266mDNS.h>
// ESP8266WebServer server(808);
// WiFiUDP Udp;
// const IPAddress outIp(192,168,1,173);
// const unsigned int outPort = 9999;
// const unsigned int localPort = 8888;
// char ssid[] = "jupiter";
// char pass[] = "qaplaopaq*604";
// // End ESP8266 definitions

// CC3000 Stuff here
// UDP
// #include "UDPServer.h"
// #define UDP_READ_BUFFER_SIZE 100
// #define LISTEN_PORT_UDP 8888
// UDPServer udpServer(LISTEN_PORT_UDP);

// CC3000 Board definitions
// #include <ccspi.h>
// #include <SPI.h>
// #include <string.h>
// #include "utility/debug.h"

// CC3000 http server definitions
// #include "utility/debug.h"
// #include "utility/socket.h"
// #define LISTEN_PORT           80      // What TCP port to listen on for connections.
// #define MAX_ACTION            10      // Maximum length of the HTTP action that can be parsed.
// #define MAX_PATH              64      // Maximum length of the HTTP request path that can be parsed.
// #define BUFFER_SIZE           MAX_ACTION + MAX_PATH + 1000  // Size of buffer for incoming request data.
// #define TIMEOUT_MS            500    // Amount of time in milliseconds to wait for

// Adafruit_CC3000_Server httpServer(LISTEN_PORT);
// uint8_t buffer[BUFFER_SIZE+1];
// int bufindex = 0;
// char action[MAX_ACTION+1];
// char path[MAX_PATH+1];
// // End httpServer setup

// #define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// #define ADAFRUIT_CC3000_VBAT  5
// #define ADAFRUIT_CC3000_CS    10
// Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS,ADAFRUIT_CC3000_IRQ,ADAFRUIT_CC3000_VBAT,SPI_CLOCK_DIVIDER); // you can change this clock speed

// #define WLAN_SSID "jupiter" // 32 characters max!
// #define WLAN_PASS "qaplaopaq*604"
// #define WLAN_SECURITY   WLAN_SEC_WPA2
// #define IDLE_TIMEOUT_MS  3000
// Amount of time to wait (in milliseconds) with no data
// received before closing the connection.  If you know the server
// you're accessing is quick to respond, you can reduce this value.

// End CC3000 definitions

unsigned long previousMillis;
int animationSpeed = 150;
int animationWidth = 3;

// void handleNotFound(){
//   String message = "File Not Found\n\n";
//   message += "URI: ";
//   message += server.uri();
//   message += "\nMethod: ";
//   message += (server.method() == HTTP_GET)?"GET":"POST";
//   message += "\nArguments: ";
//   message += server.args();
//   message += "\n";
//   for (uint8_t i=0; i<server.args(); i++){
//     message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
//   }
//   server.send(404, "text/plain", message);
// }

struct AreaStruct {
    char* title;
    int numLeds;
    uint8_t pin;
    uint8_t brightness;
    bool activated;
    int mode;
    uint8_t colorIndex;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    unsigned long previousMillis;
};

#define NUM_LEDS_AREA_1 145
#define PIN_AREA_1 7

#define NUM_LEDS_AREA_2 16
#define PIN_AREA_2 4

// #define NUM_LEDS_AREA_3 0
// #define PIN_AREA_3 3

#define NUM_AREAS 2

CLEDController *ledController[NUM_AREAS];

CRGB area0[NUM_LEDS_AREA_1];
CRGB area1[NUM_LEDS_AREA_2];
// CRGB area2[NUM_LEDS_AREA_3];

CRGB *ledArray[] = { area0,area1 };

CRGBPalette16 currentPalette;
TBlendType    currentBlending;
extern const TProgmemPalette16 WarmPalette_p PROGMEM;


struct AreaStruct area[] = {
    {
        "Living Room",
        NUM_LEDS_AREA_1,
        5,
        255,
        false,
        6
    },
    {
        "Bed Room",
        NUM_LEDS_AREA_2,
        4,
        25,
        false,
        9
    }
};
int numAreas = sizeof( area ) / sizeof( struct AreaStruct );

struct Lampstruct {
    char* title;
    int area;
    int numLeds;
    int mode;
    char* type;
    uint8_t brightness;
    int pin;
    bool relState;
    uint8_t colorIndex;
    int firstLed;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    unsigned long previousMillis;
};




struct Lampstruct lamp[]
{
    {
        "Lamp Plank 1", //Title`
        0, // Area (obsolete)
        12, // Number of LED's
        0, // Default mode
        "rgb", // Lamp type
        255 // Lamp brightness
    },
    {
        "Strip Plank 1",
        0,
        8,
        3,
        "rgb",
        255
    },
    {
        "Arabic Lamp",
        0,
        1,
        5,
        "rgb",
        255
    },
    {
        "Schemerlamp",
        0,
        20,
        0,
        "rgb",
        255
    },
    {
        "Lantaantje",
        0,
        1,
        10,
        "rgb",
        255
    },
    {
        "TV Meubel",
        0,
        34,
        5,
        "rgb",
        255
    },
    {
        "Mushroom",
        0,
        5,
        3,
        "rgb",
        255
    },
    {
        "Lamp Bas",
        0,
        11,
        3,
        "rgb",
        255
    },
    {
        "Crystal Ball",
        0,
        16,
        5,
        "rgb",
        255
    },
    {
        "Lamp Bas",
        0,
        11,
        1,
        "rgb",
        255
    },
    {
        "Ster Raam",
        0,
        1,
        9,
        "rgb",
        100
    }
};


struct Lampstruct lamp2[]
{
    {"Bedroom Lamp 1", 1, 9, 0, "rgb",255},
    {"Another Lamp", 1, 7, 1,"rgb",255}

};

// struct Lampstruct lamp3[]
// {
//     {"Kitchen Lamp", 2, 0, 0, "rgb",255},
//     {"Kitchen Test Lamp", 2, 0, 1,"rgb",255}
//
// };

struct Lampstruct *LampArray[] = {lamp, lamp2};

int a = sizeof( lamp ) / sizeof( struct Lampstruct );
int b = sizeof( lamp2 ) / sizeof( struct Lampstruct );
// int c = sizeof( lamp3 ) / sizeof( struct Lampstruct );

int numLamps[NUM_AREAS] = {a,b};

int selectedLamp = 0;
int selectedArea = 0;
bool lampControl = false;

struct ModesStruct {
    char* title;
    char* type;
    int rgb[3];
    CRGBPalette16 palette;
};

struct ModesStruct modeArray[] {
    {"Warm White", "solid", {255,130,10}}, // 0
    {"Warm 2", "solid", {255,90,0}}, // 1
    {"Greenish", "solid", {0,255,55}}, // 2
    {"Orangeish", "solid", {255,90,0}}, // 3
    {"RGB Mode", "rgb"}, // 4
    {"Party", "palette",  {255,90,0}, CRGBPalette16(PartyColors_p)}, // 5
    {"Ocean", "palette",  {255,90,0}, CRGBPalette16(OceanColors_p)}, // 6
    {"Forrest", "palette",  {255,90,0}, CRGBPalette16(ForestColors_p)}, // 7
    {"Cloud", "palette",  {255,90,0}, CRGBPalette16(CloudColors_p)}, // 8
    {"Rainbow", "palette",  {255,90,0}, CRGBPalette16(RainbowColors_p)}, // 9
    {"Heat", "palette",  {255,90,0}, CRGBPalette16(HeatColors_p)}, // 10
    {"Warm", "palette",  {255,90,0}, CRGBPalette16(WarmPalette_p)}, // 11
    {"Lava", "palette",  {255,90,0}, CRGBPalette16(LavaColors_p)}, // 12
    {"Toggle", "rel"}, // 13
    {"Timer", "rel"} // 14
};
int numModes = sizeof( modeArray ) / sizeof( struct ModesStruct );

String getSettingsJson(char* type, int id = 0, int areaId = 0) {

    DynamicJsonBuffer jsonBuffer(1900); // too large?
    JsonObject& root = jsonBuffer.createObject();

    root["animationSpeed"] = animationSpeed;
    root["animationWidth"] = animationWidth;

    if (type == "area") {
        root["type"] = type;
        area[id].activated = true;
        root["mode"] = area[id].mode;
        root["brightness"] = area[id].brightness;
        root["red"] = area[id].red;
        root["green"] = area[id].green;
        root["blue"] = area[id].blue;
    }

    if (type == "area" || type == "init") {
        root["id"] = id;
        root["numLeds"] = area[id].numLeds;
        root["title"] = area[id].title;
    }


    if (type == "lampSelect" || type == "init") {
        JsonObject& lampProperties = jsonBuffer.createObject();

        lampProperties["id"] = id;
        lampProperties["brightness"] = LampArray[areaId][id].brightness;
        lampProperties["type"] = LampArray[areaId][id].type;
        lampProperties["title"] = LampArray[areaId][id].title;
        lampProperties["numLeds"] = LampArray[areaId][id].numLeds;
        lampProperties["mode"] = LampArray[areaId][id].mode;

        if (LampArray[areaId][id].type == "rgb") {
            lampProperties["red"] = LampArray[areaId][id].red;
            lampProperties["green"] = LampArray[areaId][id].green;
            lampProperties["blue"] = LampArray[areaId][id].blue;
        }
        if (LampArray[areaId][id].type == "rel") {
            lampProperties["relaisState"] = LampArray[areaId][id].relState;
        }

        root["lampProperties"] = lampProperties;
    }

    if (type == "areaSelect" || type == "init") {
        JsonObject& areaProperties = jsonBuffer.createObject();

        areaProperties["brightness"] = area[areaId].brightness;
        areaProperties["id"] = areaId;
        // areaProperties["type"] = area[areaId].type;
        areaProperties["title"] = area[areaId].title;
        areaProperties["numLeds"] = area[areaId].numLeds;
        areaProperties["mode"] = area[areaId].mode;
        areaProperties["red"] = area[areaId].red;
        areaProperties["green"] = area[areaId].green;
        areaProperties["blue"] = area[areaId].blue;

        root["areaProperties"] = areaProperties;
    }

    if (type == "init") {

        if (area[0].activated) {
            root["numLeds"] = area[id].numLeds;
            root["title"] = area[id].title;
            root["mode"] = area[id].mode;
            root["brightness"] = area[id].brightness;
            root["red"] = area[id].red;
            root["green"] = area[id].green;
            root["blue"] = area[id].blue;
        }
        else {
            root["mode"] = LampArray[areaId][id].mode;
            root["title"] = LampArray[areaId][id].title;
            root["brightness"] = LampArray[areaId][id].brightness;
            root["red"] = LampArray[areaId][id].red;
            root["green"] = LampArray[areaId][id].green;
            root["blue"] = LampArray[areaId][id].blue;
        }

        JsonArray& areas = root.createNestedArray("areas");
        for (int i = 0; i < numAreas; i++) {
            // areas.add(area[i].title);
            JsonObject& areaarray = jsonBuffer.createObject();
            areaarray["title"] = area[i].title;
            areaarray["id"] = i;
            areaarray["brightness"] = area[i].brightness;
            areaarray["numLeds"] = area[i].numLeds;
            areaarray["activated"] = area[i].activated;
            areas.add(areaarray);
        }
        JsonArray& lamps = root.createNestedArray("lamps");
        for (int j = 0; j < NUM_AREAS; j++) {
            JsonArray& temp = jsonBuffer.createArray();

            for (int l = 0; l < numLamps[j]; l++) {
                JsonObject& temp2 = jsonBuffer.createObject();
                temp2["title"] = LampArray[j][l].title;
                temp2["id"] = l;
                temp.add(temp2);
            }
            lamps.add(temp);

        }
    }


    if (type == "modes" || type == "init") {
        JsonArray& modes = root.createNestedArray("modes");
        for (int i = 0; i < numModes; i++) {
            JsonObject& temp3 = jsonBuffer.createObject();

            temp3["title"] = modeArray[i].title;
            temp3["type"] = modeArray[i].type;

            modes.add(temp3);

        }
    }


    String output;
    root.prettyPrintTo(output);

    return output;

}


void handleHttp( WebServer &server, WebServer::ConnectionType type, char *, bool ) {

  server.httpSuccess();

  String response;


    if ( type == WebServer::POST ) {


        Serial.println("got post");
        while ( server.readPOSTparam(name, NAMELEN, value, VALUELEN) ) {

            // Serial.println( name );
            if ( String(name) == "get_settings" ) {
                Serial.println( "got seting request" );
                if ( String(value) == "init" ) {
                    response = getSettingsJson( "init" );
                    Serial.println( response );
                }
            }
        }
    }
    // webserver.print("Content-Length: \"500\"");
    server.print( response );
    delay(10);
        // if (server.method() == HTTP_POST && server.argName(0) == "get_settings") {
        //
        //     if (server.arg(0) == "init") {
        //         response = getSettingsJson("init");
        //     }
        //     if (server.arg(0) == "areaSelect") {
        //         response = getSettingsJson("areaSelect", server.arg(1   ).toInt(), server.arg(2).toInt());
        //     }
        //     if (server.arg(0) == "lampSelect") {
        //         response = getSettingsJson("lampSelect", server.arg(1).toInt(), server.arg(2).toInt());
        //     }
        //     if (server.arg(0) == "modes") {
        //         response = getSettingsJson("modes");
        //     }
        //     if (server.arg(0) == "lamps") {
        //         response = getSettingsJson("lamps" , server.arg(1).toInt());
        //     }
        //     server.send(200, "text/plain", response);
        // }
}






//////////////////////
//
//
//
//
//
//////////////////////
                    //
                    //
                    //
                    //
                    //
//////////////////////









void setup() {
    Serial.begin(115200);
    Serial.println("Led's begin...");

    Ethernet.begin(mac, ip);

    webserver.setDefaultCommand(&handleHttp);
    webserver.begin();

    Serial.print("server is at ");
    Serial.println(Ethernet.localIP());

    Udp.begin(udpPort);


    ledController[0] = &FastLED.addLeds<WS2812B, PIN_AREA_1, GRB>(area0, NUM_LEDS_AREA_1).setCorrection( UncorrectedColor );
    ledController[1] = &FastLED.addLeds<WS2812B, PIN_AREA_2, GRB>(area1, NUM_LEDS_AREA_2).setCorrection( UncorrectedColor );
    // ledController[2] = &FastLED.addLeds<WS2812B, PIN_AREA_3, GRB>(area2, NUM_LEDS_AREA_3).setCorrection( UncorrectedColor );

    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;

    delay(2000);
    calculateFirstLed();


    Serial.println("\r\nArea Information\r\n");

    for (uint8_t i = 0; i < numAreas; i++) {
        Serial.println("Area " + String(i) + " > " + String(area[i].title));
        Serial.println("# Leds " + String(area[i].numLeds));
        Serial.println("Pin " + String(area[i].pin));
        Serial.println("-------------------------------------");
    }
}





//
//
//
//
//
//
//
//
//
/////////////////






void loop() {


    int firstLed;
    int lastLed;
    uint8_t brightness;
    int mode;
    int id;
    int areaId;
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */

    for (int i = 0; i < NUM_AREAS; i++) {

        if (area[i].activated) {
            firstLed = 0;
            brightness = area[i].brightness;
            lastLed = area[i].numLeds;
            mode = area[i].mode;
            id = i;
            areaId = i;
            lightEmUp(firstLed, lastLed, brightness,mode, id, areaId, "area", startIndex, area[i].activated);
        }
        else {
            for (int h = 0; h < numLamps[i]; h++) {
                firstLed = LampArray[i][h].firstLed;
                lastLed = LampArray[i][h].firstLed + LampArray[i][h].numLeds;
                brightness = LampArray[i][h].brightness;
                mode = LampArray[i][h].mode;
                id = h;
                areaId = i;
                lightEmUp(firstLed, lastLed, brightness,mode, id, areaId, "lamp", startIndex, area[i].activated);
            }
        }
    }


    unsigned long start = millis();
	while((millis()-start) < animationSpeed) {
        #ifndef FASTLED_ACCURATE_CLOCK
		// make sure to allow at least one ms to pass to ensure the clock moves
		// forward
		delay(1);
        #endif
        ledController[0]->showLeds(int(area[0].brightness));
        ledController[1]->showLeds(int(area[1].brightness));
        // ledController[2]->showLeds(int(area[2].brightness));
        receiveOsc();

        // handle web
        // char buff[16];
        // int len = 16;
        // webserver.processConnection(buff, &len);
        webserver.processConnection();

        // // ESP8266 process webserver requests
        // server.handleClient();
	}


}

void calculateFirstLed() {
    int count = 0;

    Serial.println("\r\nCalculating 1st LED\r\n");
    Serial.println("-----------------------------\r\n");

    for(int h = 0; h < NUM_AREAS; h++){
        Serial.println("Area :" + String(h) + " > " + area[h].title);

        count = 0;
        for(int i = 0; i < numLamps[h]; i++){
            Serial.println("\r\n-----------------------------\r\n");
            Serial.println("Title: " + String(LampArray[h][i].title));
            Serial.println("Id: " + String(i));
            Serial.println("Area: " + String(h));

            LampArray[h][i].firstLed = count;
            count = count + LampArray[h][i].numLeds;

            Serial.println("Number of LED's :" + String(LampArray[h][i].numLeds));
            Serial.println("1st LED set @ pos " + String(LampArray[h][i].firstLed));
        }
        Serial.println("-- End of Area --------------\r\n");
    }
    Serial.println("\r\nEnd calculating 1st LED\r\n");
}
void setColor(int areaId, int led, int key, int mode, uint8_t colorIndex, uint8_t red, uint8_t green, uint8_t blue)  {

    if (mode == 0) {
        ledArray[ areaId ][led].setRGB( 0, 68, 221);
    } else if (mode == 1) {
        ledArray[ areaId ][led].setRGB( 255, 68, 90);
    } else if (mode == 2) {
        ledArray[ areaId ][led].setRGB( 100, 255, 0);
    } else if (mode == 3) {
        ledArray[ areaId ][led].setRGB( 255, 55, 0);
    } else if (mode == 4) {
        ledArray[ areaId ][led].setRGB( red, green, blue);
    } else if (mode == 5) {
        ledArray[ areaId ][ led ] = ColorFromPalette( currentPalette, colorIndex, 255, currentBlending);
    }
}
void lightEmUp(int firstLed, int lastLed, uint8_t brightness, int mode, int id, int areaId, char* type, uint8_t colorIndex, bool areaActivated) {
    CRGBPalette16 palette;
    int red, green, blue;

    if (areaActivated) {
        brightness = 255;
    }

    if (modeArray[mode].type == "solid") {
        red = map(modeArray[mode].rgb[0], 0, 255, 0, brightness);
        green = map(modeArray[mode].rgb[1], 0, 255, 0, brightness);
        blue = map(modeArray[mode].rgb[2], 0, 255, 0, brightness);
    }
    else if (modeArray[mode].type == "rgb") {
        red = map(LampArray[areaId][id].red, 0, 255, 0, brightness);
        green = map(LampArray[areaId][id].green, 0, 255, 0 ,brightness);
        blue = map(LampArray[areaId][id].blue, 0, 255, 0, brightness);
    }
    else if (modeArray[mode].type == "palette") {
        // palette = modeArray[mode].palette;
    }

    if (modeArray[mode].type == "palette") {
        // if (millis() - previousMillis > 300) {
            for (uint8_t i = firstLed; i < lastLed; i++) {
                ledArray[ areaId ][ i ] = ColorFromPalette( modeArray[mode].palette, colorIndex, brightness, currentBlending);
                colorIndex += animationWidth;
            }
        //     previousMillis = millis();
        // }
    }
    else {
        for (uint8_t i = firstLed; i < lastLed; i++) {
            ledArray[ areaId ][i].setRGB( red, green, blue);
        }
    }

}
void receiveOsc() {

    // Normal OSC stuff

    // // CC3000 listening UDP
    // OSCMessage  bundle;
    // int size = Udp.parsePacket();
    //
    // if (size > 0) {
    //     while (size--) {
    //         bundle.fill(Udp.read());
    //     }


    // if (udpServer.available()) {
  if (Udp.parsePacket()) {
        OSCBundle bundle;

       int size = Udp.parsePacket();

       if (size > 0) {
           Serial.println("----------" + String(size));
        //    while (size--) {
            //    Serial.println( size );
               bundle.fill( Udp.read(), size);

            //    Serial.print(buffer[counter]);
        //    }
            if (!bundle.hasError()) {

                bundle.send(Serial);

                // Serial.println("----------\n" + String(buffer));

                bundle.route("/bri", bri);
                bundle.route("/mod", mod);
                bundle.route("/red", red);
                bundle.route("/grn", grn);
                bundle.route("/blu", blu);
                bundle.dispatch("/sel", sel);
                bundle.dispatch("/rel", rel);
                bundle.dispatch("/are", are);
                bundle.dispatch("/spd", spd);
                bundle.dispatch("/width", width);
                Serial.println("OSC receive: ");
            } else {
                error = bundle.getError();
                Serial.print("error: ");
                Serial.println(error);
            }
        }
    }
}

void sel(OSCMessage &msg) {
    int selected = msg.getFloat(0);
    // lampControl = true;
    area[selected].activated = false;
    // Serial.print("/sel: ");
    // Serial.println(selectedLamp);
}

void spd(OSCMessage &msg) {
    animationSpeed = msg.getFloat(0);
    // Serial.print("/spd: ");
}

void width(OSCMessage &msg) {
    animationWidth = msg.getFloat(0);
    // Serial.print("/spd: ");
}

void mod(OSCMessage &msg, int offSet) {
    Serial.println("Setting mode");

    int mode = msg.getFloat(0);

    char address[15];
    msg.getAddress(address, 0);

    char* addressParts[4]; // 0:bir,1:lamp/area,2:areaId,3:lampId
    uint8_t c = 0;
    for (char *p = strtok(address,"/"); p != NULL; p = strtok(NULL, "/")) {
        if (p) {
            addressParts[c] = p;
            Serial.println(p);
        }
      c++;
    }

    int ar = atoi(addressParts[2]);
    int la = atoi(addressParts[3]);

    // Serial.print("/mod: ");
    if (String(addressParts[1]) == "lamp") {
        area[ar].activated = false;
        LampArray[ar][la].mode = mode;
    }
    else {
        Serial.println("not lamp but " + String(addressParts[1]));
        area[ar].mode = mode;
        area[ar].activated = true;
    }
}

void red(OSCMessage &msg, int offSet) {
    int r = msg.getFloat(0);

    char address[15];
    msg.getAddress(address, 0);
    char* addressParts[4]; // 0:type,1:lamp/area,2:areaId,3:lampId

    uint8_t c = 0;
    for (char *p = strtok(address,"/"); p != NULL; p = strtok(NULL, "/")) {
        if (p) {
            addressParts[c] = p;
        }
      c++;
    }

    int areaId = atoi(addressParts[2]);
    int lampId = atoi(addressParts[3]);

    Serial.println("Done destructing path: " + String(address));
    if (String(addressParts[1]) == "lamp") {
        LampArray[areaId][lampId].red = r;
    }
    else {
        area[areaId].red = r;
    }

}
void grn(OSCMessage &msg, int offSet) {
    int g = msg.getFloat(0);

    char address[15];
    msg.getAddress(address, 0);
    char* addressParts[4]; // 0:type,1:lamp/area,2:areaId,3:lampId

    uint8_t c = 0;
    for (char *p = strtok(address,"/"); p != NULL; p = strtok(NULL, "/")) {
        if (p) {
            addressParts[c] = p;
        }
      c++;
    }

    int areaId = atoi(addressParts[2]);
    int lampId = atoi(addressParts[3]);

    Serial.println("Done destructing path: " + String(address));
    if (String(addressParts[1]) == "lamp") {
        LampArray[areaId][lampId].green = g;
    }
    else {
        area[areaId].green = g;
    }
}


void blu(OSCMessage &msg, int offSet) {

    Serial.println("got bluzzz");
    int b = msg.getFloat(0);

    char address[15];
    msg.getAddress(address, 0);
    char* addressParts[4]; // 0:type,1:lamp/area,2:areaId,3:lampId

    uint8_t c = 0;
    for (char *p = strtok(address,"/"); p != NULL; p = strtok(NULL, "/")) {
        if (p) {
            addressParts[c] = p;
        }
      c++;
    }

    int areaId = atoi(addressParts[2]);
    int lampId = atoi(addressParts[3]);

    Serial.println("Done destructing path: " + String(address));
    if (String(addressParts[1]) == "lamp") {
        LampArray[areaId][lampId].blue = b;
    }
    else {
        area[areaId].blue = b;
    }
}


void bri(OSCMessage &msg, int offSet) {
    int b = msg.getFloat(0);
    // char areaFromAddress[10];
    // int idFromAddress;
    // msg.getAddress(areaFromAddress,10);
    char address[15];
    msg.getAddress(address, 0);

    char* addressParts[4]; // 0:bir,1:lamp/area,2:areaId,3:lampId
    uint8_t c = 0;
    for (char *p = strtok(address,"/"); p != NULL; p = strtok(NULL, "/")) {
        if (p) {
            addressParts[c] = p;
        }
      c++;
    }

    int areaId = atoi(addressParts[2]);
    int lampId;
    // int lampId = 0;
    // Serial.println("\r\ngot area ID: " + String(areaId));
    // Serial.println("got lamp ID: " + String(lampId));
    // Serial.println("path 1" + String(addressParts[1]));

    if (String(addressParts[1]) == "area"){
        // Serial.println("Setting");
        area[areaId].brightness = int(b);
    }
    else {
        lampId = atoi(addressParts[3]);
        LampArray[areaId][lampId].brightness = int(b);
    }

    // if (msg.match("/bri/lamp") == 9){
    //     Serial.println("Setting");
    //     area[0].brightness = b;
    // }

}
void are(OSCMessage &msg) {
    lampControl = false;
    selectedArea = msg.getFloat(0);
    area[selectedArea].activated = true;
}

void rel(OSCMessage &msg) {
    int state = msg.getFloat(0);
    if (state == 0) {
        digitalWrite(2, LOW);
    }
    else {
        digitalWrite(2,HIGH);
    }
}

void setLampBrightness(int brightness, int areaId, int lampId) {
    int firstLed = LampArray[ areaId ][ lampId ].firstLed;
    int numLeds = LampArray[ areaId ][ lampId ].numLeds;

    for (int i = firstLed; i < firstLed + numLeds; i++) {
        ledArray[ areaId ][ i ].fadeToBlackBy( brightness );
        // ledArray[ areaId ][ i ] %= brightness;
        // Serial.println("\r\n1st: " + String(firstLed));
        // Serial.println("Num: " + String(numLeds));
        // Serial.println("bri: " + String(brightness));
    }
}

// Color palettes
const TProgmemPalette16 WarmPalette_p PROGMEM =
{

    CRGB::Orange,
    CRGB::Orange,
    CRGB::Orange,
    CRGB::Red,
    CRGB::Red,
    CRGB::Red,
    CRGB::Orange,
    CRGB::Orange,
    CRGB::Orange,
    CRGB::Yellow,
    CRGB::Yellow,
    CRGB::Orange,
    CRGB::OrangeRed,
    CRGB::Orange,
    CRGB::Yellow,
    CRGB::Red
};
